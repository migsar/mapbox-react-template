import React, { useState } from 'react';
import ReactMapGL, { ViewState } from 'react-map-gl';
import mapStyle from 'services/mapbox/style-spec.json';
import './my-map.css';

const { REACT_APP_MAPBOX_TOKEN: MapboxToken } = process.env;

const MyMap: React.FC = () => {
  const initialMapState: ViewState = {
    zoom: 14,
    latitude: -6.1646383,
    longitude: 39.1986324
  };
  const [mapState, setMapState] = useState(initialMapState);
  const viewportChangeHandler = (viewport: ViewState) => setMapState({...viewport});

  return (
    <div style={{width: '100vw', height: '100vh'}}>
      <ReactMapGL
        {...mapState}
        mapStyle={mapStyle}
        width="100%"
        height="100%"
        mapboxApiAccessToken={MapboxToken}
        onViewportChange={viewportChangeHandler}
      />
    </div>
  );
}

export default MyMap;
