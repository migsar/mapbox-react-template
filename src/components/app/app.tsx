import React from 'react';
import MyMap from 'components/my-map/my-map';
import './app.css';

const App: React.FC = () => {
  return (
    <div className="App">
      <MyMap />
    </div>
  );
}

export default App;
